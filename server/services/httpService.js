const axios = require('axios');
const { API_KEY, endPoints } = require('../constants');

module.exports = {
  getRigorData: function(query) {
    return axios({
      method: 'GET',
      url:`${endPoints.rigor}${query}`,
      headers: {
        'API-KEY': API_KEY,
        'Content-Type': 'application/json'
      }
    });
  },
}

