const express = require('express');
const axios = require('axios');
const router = express.Router();
const { getRigorData } = require('../services/httpService');
const { DASHBOARD_ID } = require('../constants');
const rigorChecksMiddleware = require('./rigorChecksMiddleware');


router.get('/rigorKPIs', (req, res, next) => {
  const { start, end, metrics } = req.query;
  const metricsQuery = metrics.split(',').reduce((acc, curr) => `${acc}metrics[]=${curr}&`, '');
  const query = `/checks/real_browsers/${DASHBOARD_ID}/performance_kpis/data?from=${start}&to=${end}&${metricsQuery}group_by[]=page`;

  getRigorData(query)
    .then(result => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(result.data));
    })
    .catch(err => {
      console.log(err);
      console.log('==============================================================================err');
      next(err)
    })
});

router.get('/rigorChecks', rigorChecksMiddleware, (req, res, next) => {
  const hars = req.resultData.reduce((a, c) => {
    const homePage = c.pages.find(page => page.url === 'https://qa3.sephora.com/');
    return a.concat(axios(homePage.har_url))
  }, [])

  Promise.all(hars).then(result => {
    res.send(JSON.stringify(result.reverse().map(({ data }) => data )));
  }).catch(err => next(err))

});

module.exports = router;
