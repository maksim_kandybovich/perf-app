import * as moment from 'moment';

export const populatePoints = (prev, curr) => {
  if (prev) {
    return [...prev.pageRendered, Math.ceil(curr) || 0]
  }

  return [curr.pageRendered || 0]
}

export const isOriginUrl = url => !/^https:\/\/qa3.sephora.com/g.test(url) ||
  url === 'https://qa3.sephora.com/serverInfo' ||
  url === 'https://qa3.sephora.com/about-us' ||
  url === 'https://qa3.sephora.com/images/favicon.ico'

export const getParameterByName = (name, url) => {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

export function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export const getPageType = url => getParameterByName('page-type', url);
export const getEntriesByType = (data = [], type) => {
  const res = data.find(({ name }) => name === type) || 0;
  return Math.round(Number(res.value) || 0);
};
export const createQuery = data => data.split(',').reduce((acc, curr) => `${acc}metrics[]=${curr}&`, '')
export const sum = (...args) => args.reduce((acc, curr) => acc + (Number(curr) || 0), 0);
export const convertToKb = bytes => Math.round(0.001 * bytes);

export function compareDates(left, right) {
  return moment(right).diff(moment(left), 'minutes') < 0;
}

export function sortByDate(data) {
  return Object.keys(data).reduce((acc, curr) => {
    const points = [...data[curr].points];
    const labels = [...data[curr].labels];

    const labelsLength = labels.length;
    for(let i = 0; i < labelsLength; i++) {
      let key = labels[i];
      let pointsKey = points[i];
      let j = i - 1;

      while(j >= 0 && compareDates(labels[j], key)) {
        labels[j + 1] = labels[j];
        points[j + 1] = points[j];
        j = j - 1;
      }

      labels[j + 1] = key;
      points[j + 1] = pointsKey;
    }

    return {
      ...acc,
      [curr]: {
        ...data[curr],
        points,
        labels
      }
    }
  }, {})
}

export const getBundleData = (prevData, data, bundleName) => {
  const response = data.log.entries.find(entry => entry.request.url.indexOf(bundleName) !== -1);
  if(!response) {
    return prevData[bundleName] || {};
  }
  const points = convertToKb(response.response.bodySize);
  const labels = moment(data.log.pages[0].startedDateTime).format('YYYY-MM-DD');
  return {
    url: data.log.pages[0].title,
    points: prevData[bundleName] && prevData[bundleName].points ? prevData[bundleName].points.concat(points) : [points],
    labels: prevData[bundleName] && prevData[bundleName].labels ? prevData[bundleName].labels.concat(labels) : [labels],
  }
}

export const mergeByUrl = (data, initial) => Object.keys(data).reduce((acc, curr) => {
  const field = data[curr];
  return {
    ...acc,
    [curr]: acc[curr] ?
      {
        labels: [...acc[curr].labels, ...field.labels],
        points: [...acc[curr].points, ...field.points],
        pageGroup: field.pageGroup,
      }
      :
      {
        labels: [...field.labels],
        points: [...field.points],
        pageGroup: field.pageGroup,
      }
  }
}, initial);
