import * as moment from 'moment';
import {COMPONENTS_BUNDLE, POSTLOAD_BUNDLE, PRIORITY_BUNDLE} from '../constants';
import {
  getBundleData,
  isOriginUrl,
  getEntriesByType,
  mergeByUrl,
  sortByDate,
  convertToKb,
  sum
} from '../utils';

export const populateBundleSize = data => data.reduce((a, c) => {
  return {
    ...a,
    [COMPONENTS_BUNDLE]: getBundleData(a, c, COMPONENTS_BUNDLE),
    [POSTLOAD_BUNDLE]: getBundleData(a, c, POSTLOAD_BUNDLE),
    [PRIORITY_BUNDLE]: getBundleData(a, c, PRIORITY_BUNDLE),
  }
}, {})

export const populatePageRender = ({ series }, isAvgByDay) => {
  const result = series.reduce((acc, curr) => {

    let data = curr.data.reduce((prev, currItem) => {
      const [ pageUrl, params ] = currItem.url.split('?');

      if (isOriginUrl(pageUrl)) {
        return prev;
      }

      const urlParams = new URLSearchParams(params);
      const pageGroup = urlParams.get('page-template') || '';
      const url = pageGroup || pageUrl;

      return {
        ...prev,
        byUrl: {
          ...prev.byUrl,
          [url]: populateByUrl(
            prev.byUrl[url],
            currItem,
            getEntriesByType(currItem.custom_timings, 'Page Rendered')
          )
        }
      }

    }, { byUrl: {} });

    return mergeByUrl(data.byUrl, acc)

  }, {});

  return isAvgByDay ? getAvgByDay(sortByDate(result)) : sortByDate(result)
}

export const populatePageSize = ({ series }, isAvgByDay) => {
  const result = series.reduce((acc, curr) => {
    let data = curr.data.reduce((prev, currItem) => {
      if (convertToKb(currItem.html_bytes) < 5) {
        return prev
      }

      const [ pageUrl, params ] = currItem.url.split('?');

      if (isOriginUrl(pageUrl)) {
        return prev;
      }

      const urlParams = new URLSearchParams(params);
      const pageGroup = urlParams.get('page-template') || '';
      const url = pageGroup || pageUrl;

      return {
        ...prev,
        byUrl: {
          ...prev.byUrl,
          [url]: populateByUrl(
            prev.byUrl[url],
            currItem,
            convertToKb(sum(currItem.html_bytes))
          )
        }
      }

    }, { byUrl: {} });

    return mergeByUrl(data.byUrl, acc)

  }, {});

  return isAvgByDay ? getAvgByDay(sortByDate(result)) : sortByDate(result)
}

export const populateSpeedIndex = ({ series }, isAvgByDay) => {
  const result = series.reduce((acc, curr) => {
    const data = curr.data.reduce((prev, currItem) => {
      const [ pageUrl, params ] = currItem.url.split('?');

      if (isOriginUrl(pageUrl)) {
        return prev;
      }

      const urlParams = new URLSearchParams(params);
      const pageGroup = urlParams.get('page-template') || '';
      const url = pageGroup || pageUrl;

      return {
        ...prev,
        byUrl: {
          ...prev.byUrl,
          [url]: populateByUrl(
            prev.byUrl[url],
            currItem,
            currItem.speed_index
          )
        }
      }
    }, { byUrl: {} })

    return mergeByUrl(data.byUrl, acc)

  }, {});

  return isAvgByDay ? getAvgByDay(sortByDate(result)) : sortByDate(result)
}

function populateByUrl(byUrl, currItem, metric) {
  let urlParams = new URLSearchParams(currItem.url.split('?')[1]);
  const pageGroup = urlParams.get('page-template') || '';
  return byUrl ?
    {
      labels: [...byUrl.labels, currItem.time],
      points: [...byUrl.points, metric],
      pageGroup: byUrl.pageGroup && pageGroup
    }
    :
    {
      labels: [currItem.time],
      points: [metric],
      pageGroup
    }
};

function getAvgByDay(byUrl) {
  return Object.keys(byUrl).reduce((acc, curr) => {
    let points = [];
    let pointForSlice = [];

    const labels = byUrl[curr].labels.reduce((prevPoints, currPoint, index) => {
      const formattedPoint = moment(currPoint).format('YYYY-MM-DD');
      if (formattedPoint === prevPoints[prevPoints.length - 1]) {
        return prevPoints
      }
      pointForSlice.push(index);
      return [...prevPoints, formattedPoint];
    }, []);

    pointForSlice.push(byUrl[curr].labels.length);

    pointForSlice.reduce((prevSlice, currSlice) => {
      points.push(
        Math.round(byUrl[curr].points.slice(prevSlice, currSlice).reduce((a, c = 0) => a + c, 0) / (currSlice - prevSlice))
      );

      return currSlice;
    });

    return {
      ...acc,
      [curr]: {
        labels,
        points
      }
    }
  }, {  })
}
