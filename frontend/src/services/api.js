import {queryMap} from '../constants';

export function callApi(start, end, activeTab) {
  if (activeTab === 'bundleSize') {
    return fetch(`api/rigorChecks?start=${start}&end=${end}`).then(response => {
      if(response.status === 200) {
        return response.json()
      }
      return Promise.reject('err')
    })
  }
  const metrics = queryMap[activeTab] || 'speed_index';
  return fetch(`api/rigorKPIs?start=${start}&end=${end}&metrics=${metrics}`).then(response => {
    if(response.status === 200) {
      return response.json()
    }
    return Promise.reject()
  })
}
