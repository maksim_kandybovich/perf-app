export const COMPONENTS_BUNDLE = 'components.chunk';
export const POSTLOAD_BUNDLE = 'postload.chunk';
export const PRIORITY_BUNDLE = 'priority.bundle';

export const queryMap = {
  speedIndex: 'speed_index',
  avgPageSize: 'html_bytes,image_bytes,javascript_bytes,css_bytes,video_bytes,other_bytes',
  renderTime: 'mark.Page+Rendered',
};

export const bundlesColors = {
  [POSTLOAD_BUNDLE]: 'red',
  [PRIORITY_BUNDLE]: 'blue',
  [COMPONENTS_BUNDLE]: 'green',
};

export const chartOptions = {
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  }
}
