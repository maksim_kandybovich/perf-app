import React from 'react';
import classNames from 'classnames';
import styles from './Tabs.module.css';

function Tabs({ activeTab, onTabClick }) {
  return (
    <div>
      <button
        className={classNames({
          [styles.tab]: true,
          [styles.tabActive]: activeTab === 0,
        })}
        onClick={() => onTabClick(0, 'speedIndex')}
      >
        Speed Index
      </button>
      <button
        className={classNames({
          [styles.tab]: true,
          [styles.tabActive]: activeTab === 1,
        })}
        onClick={() => onTabClick(1, 'avgPageSize')}
      >
        Avg Page Size
      </button>
      <button
        className={classNames({
          [styles.tab]: true,
          [styles.tabActive]: activeTab === 2,
        })}
        onClick={() => onTabClick(2, 'renderTime')}
      >
        Render Time
      </button>
      <button
        className={classNames({
          [styles.tab]: true,
          [styles.tabActive]: activeTab === 3,
        })}
        onClick={() => onTabClick(3, 'bundleSize')}
      >
        Bundle size
      </button>
    </div>
  )
}

export default React.memo(Tabs);
