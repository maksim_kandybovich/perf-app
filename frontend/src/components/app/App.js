import React, {useState, useEffect} from 'react';
import * as moment from 'moment';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import {callApi} from '../../services/api';
import Tabs from '../tabs/Tabs';
import {
  SpeedIndex,
  AvgPageSize,
  RenderTime,
  BundleSize
} from '../charts';
import 'bootstrap-daterangepicker/daterangepicker.css';


function useCallApi(activeTab, {currMetric, startDate, endDate }) {
  const [isFetching, setIsFetching] = useState(false);
  const [byQuery, memoResponse] = useState(false);
  const query = [currMetric, startDate, endDate].join(',');
  useEffect(() => {
    if(!byQuery[query]) {
      setIsFetching(true);
      callApi(
        startDate,
        endDate,
        currMetric,
      ).then(json => {
        setIsFetching(false);
        memoResponse({ ...byQuery, [query]: json })
      }).catch(err => {
        setIsFetching(false);
      })
    }
  }, [query]);

  return { isFetching, response: byQuery[query] };
}

function App() {
  const [activeTab, setActiveTab] = useState(0);
  const [currMetric, setCurrMetric] = useState('speedIndex');
  const [datesRange, setDates] = useState({
    startDate: moment().add(-2, 'day'),
    endDate: moment()
  });
  const { isFetching, response } = useCallApi(
    activeTab,
    {
      currMetric,
      startDate: datesRange.startDate.format('YYYY-MM-DD'),
      endDate: datesRange.endDate.format('YYYY-MM-DD'),
    }
  );

  function setTab(index, metric) {
    setActiveTab(index);
    setCurrMetric(metric)
  }

  function applyDates(event, picker) {
    setDates({
      startDate: picker.startDate,
      endDate: picker.endDate
    })
  }

  return (
    <div>
      <DateRangePicker startDate={datesRange.startDate} endDate={datesRange.endDate} onEvent={applyDates}>
        <input type="text" value={`${datesRange.startDate.format('MM/DD/YYYY')} - ${datesRange.endDate.format('MM/DD/YYYY')}`}/>
      </DateRangePicker>
      <Tabs onTabClick={setTab} activeTab={activeTab} />
      <div>
        <div>
          {(activeTab === 0) &&
            <SpeedIndex data={response} isFetching={isFetching} />
          }
        </div>
        <div>
          {(activeTab === 1) &&
            <AvgPageSize data={response} isFetching={isFetching} />
          }
        </div>
        <div>
          {(activeTab === 2) &&
            <RenderTime data={response} isFetching={isFetching} />
          }
        </div>
        <div>
          {(activeTab === 3) &&
            <BundleSize data={response} isFetching={isFetching} />
          }
        </div>
      </div>
    </div>
  )
}

export default App;
