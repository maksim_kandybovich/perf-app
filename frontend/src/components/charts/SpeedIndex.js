import React from 'react';
import {populateSpeedIndex} from '../../services/populate';
import DefaultChart from './DefaultChart';

function SpeedIndex({data, isFetching}) {
  if (isFetching || !data) {
    return <div>Fetching...</div>
  }
  return <DefaultChart data={populateSpeedIndex(data, true)} title='Speed index' />
}

export default React.memo(SpeedIndex);
