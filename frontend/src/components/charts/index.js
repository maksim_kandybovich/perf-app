export {default as SpeedIndex} from './SpeedIndex';
export {default as AvgPageSize} from './AvgPageSize';
export {default as RenderTime} from './RenderTime';
export {default as BundleSize} from './BundleSize';
