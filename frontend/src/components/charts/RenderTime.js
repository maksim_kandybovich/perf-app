import React from 'react';
import {populatePageRender} from '../../services/populate';
import MixedChart from './MixedChart';

function RenderTime({data, isFetching}) {
  if (isFetching || !data) {
    return <div>Fetching...</div>
  }
  const populatedData = populatePageRender(data, true);

  return <MixedChart data={populatedData} title="Render Time" />
}

export default React.memo(RenderTime);
