import React from 'react';
import {bundlesColors} from '../../constants';
import {populateBundleSize} from '../../services/populate';
import MixedChart from './MixedChart';

function BundleSize({data, isFetching}) {
  if (isFetching || !data) {
    return <div>Fetching...</div>
  }
  const populatedData = populateBundleSize(data, true);
  return <MixedChart data={populatedData} colorsMap={bundlesColors} title="Bundle size" />
}

export default React.memo(BundleSize);
