import React from 'react';
import {populatePageSize} from '../../services/populate';
import DefaultChart from './DefaultChart';

function AvgPageSize({data, isFetching}) {
  if (isFetching || !data) {
    return <div>Fetching...</div>
  }
  return <DefaultChart data={populatePageSize(data, true)} title='Average page size' />
}

export default React.memo(AvgPageSize);
