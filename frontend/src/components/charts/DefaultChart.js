import React from 'react';
import {Line} from 'react-chartjs-2';
import {chartOptions} from '../../constants';
import {createDataSet} from './createDataSet';

function DefaultChart({data, title}) {
  return (
    Object.keys(data).map((pageUrl) => {
      const chartData = createDataSet(
        data[pageUrl].labels,
        data[pageUrl].points,
        title
      );
      return (
        <div>
          <h2>{pageUrl}</h2>
          <Line data={chartData} options={chartOptions} />
        </div>
      )
    })
  )
}

export default React.memo(DefaultChart);
