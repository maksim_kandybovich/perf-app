import React from 'react';
import {Line} from 'react-chartjs-2';
import {chartOptions, bundlesColors} from '../../constants';
import {getRandomColor} from '../../utils';

function MixedChart({data, title, colorsMap}) {

  let labels = null;
  const datasets = Object.keys(data).reduce((acc, curr) => {

    if (!labels) {
      labels = data[curr].labels
    }
    return [
      ...acc,
      {
        label: `${data[curr].pageGroup || curr}`,
        data: data[curr].points,
        borderColor: [
          colorsMap ? colorsMap[curr] : getRandomColor(),
        ],
        backgroundColor: [
          'rgba(255, 99, 132, 0)',
        ],
        borderWidth: 2,
        pointHitRadius: 10,
        pointBorderColor: 'rgba(0, 84, 87, 0)',
        cubicInterpolationMode: 'monotone'
      }
    ]
  }, []);

  const chartData = {
    labels,
    datasets
  }

  return (
    <div>
      <h2>{ title }</h2>
      <Line data={chartData} options={chartOptions} />
    </div>
  )
}

export default React.memo(MixedChart);
